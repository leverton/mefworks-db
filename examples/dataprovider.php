<?php

require_once __DIR__ . '/../vendor/autoload.php';

use mef\Db\Driver\DataProviderDriver;
use mef\Db\Driver\DataProvider\SequentialArrayDataProvider;

/**
 * The DataProviderDriver is primarily used as a way to provide a fake driver
 * for unit testing purposes.
 *
 * The ArrayDataProvider and SequentialArrayDataProvider implementations simply
 * return a hardcoded array for each query. While ArrayDataProvider always
 * returns the same data, the SequentialArrayDataProvider can return a
 * different set for each query, based on its index.
 */

$dataProvider = new SequentialArrayDataProvider([
	1 => [
		['id' => '1', 'english' => 'one', 'spanish' => 'uno'],
		['id' => '2', 'english' => 'two', 'spanish' => 'dos']
	],
	2 => [
		['english' => 'one'],
		['english' => 'two']
	],
	3 => [
		['id' => '1', 'english' => 'one'],
		['id' => '2', 'english' => 'two']
	],
	4 => [
		['id' => '1', 'english' => 'one', 'spanish' => 'uno'],
		['id' => '2', 'english' => 'two', 'spanish' => 'dos']
	],
]);

$driver = new DataProviderDriver($dataProvider);

// Will be empty because index 0 does not exist and there were no prior ones
// to fall back to using.
echo 'Query #0', PHP_EOL;
var_dump($driver->query('SELECT * FROM word')->fetchAll());

// Will return the complete resultset (the actual SQL used is irrelevant)
echo 'Query #1', PHP_EOL;
var_dump($driver->query('SELECT * FROM word')->fetchAll());

// fetchKeyed() with a single column will return a col1 => true pair.
echo 'Query #2', PHP_EOL;
var_dump($driver->query('SELECT english FROM word')->fetchKeyed());

// fetchKeyed() with two columns will return a col1 => col2 pair.
echo 'Query #3', PHP_EOL;
var_dump($driver->query('SELECT id,english FROM word')->fetchKeyed());

// fetchKeyed() with three or more columns will return the remainder of the
// columns as an associative array.
echo 'Query #4', PHP_EOL;
var_dump($driver->query('SELECT * FROM word')->fetchKeyed());

// The remainder of the queries will be the same as Query #4 since it is the
// last one specified.
echo 'Query #5', PHP_EOL;
var_dump($driver->query('SELECT * FROM word')->fetchColumn(2));

// The remainder of the queries will be the same as Query #4 since it is the
// last one specified.
echo 'Query #5', PHP_EOL;
foreach ($driver->query('SELECT * FROM word')->getKeyedCallbackIterator(
	function($row) {
		return [$row['id'],  (object)[
			'english' => strtoupper($row['english']),
			'spanish' => strtoupper($row['spanish'])
		]];
	}) as $id => $word)
{
	echo $id, "\t", $word->english, "\t", $word->spanish, "\n";
}