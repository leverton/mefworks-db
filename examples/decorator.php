<?php

require_once __DIR__ . '/../vendor/autoload.php';

use mef\Db\Driver\PdoDriver;
use mef\Db\TransactionDriver\NestedTransactionDriver;

/**
 * By extending the AbstractDecoratorDriver class, you can add methods to an
 * existing driver.
 *
 * The mef\Sql packages uses this to extend drivers with "natural language"
 * SQL building methods.
 *
 * e.g., $db->insert()->into('foo')->namedValues(['key' => 1])
 *
 * This example just adds three contrived methods.
 */

class MyDecorator extends mef\Db\Driver\AbstractDecoratorDriver
{
	public function createTest()
	{
		$this->execute('CREATE TABLE test ("id" INTEGER PRIMARY KEY, "key" TEXT, "value" TEXT)');
	}

	public function insertTest($key, $value)
	{
		$this->prepare('INSERT INTO test (key,value) VALUES (?,?)', [$key, $value])->execute();
	}

	public function selectTest($key)
	{
		return $this->prepare('SELECT value FROM test WHERE key=?', [$key])->query()->fetchValue();
	}
}

$pdo = new PDO('sqlite::memory:');

$pdoDriver = new PdoDriver($pdo);
$transactionEngine = new NestedTransactionDriver($pdoDriver);
$pdoDriver->setTransactionDriver($transactionEngine);

$driver = new MyDecorator($pdoDriver);
$driver->createTest();
$driver->startTransaction();
$driver->insertTest('foo', 'bar');
$driver->commit();

echo $driver->selectTest('foo'), PHP_EOL;