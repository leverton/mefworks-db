<?php
use mef\Db\Driver\DriverInterface;
use mef\Db\TransactionDriver\EmulatedNestedTransactionDriver;
use mef\Db\TransactionDriver\Exception\CommitException;
use mef\Db\TransactionDriver\Exception\TransactionNotStartedException;

/**
 * @coversDefaultClass mef\Db\TransactionDriver\EmulatedNestedTransactionDriver
 */
class EmulatedNestedTransactionDriverTest extends \mef\Db\Test\AbstractTest
{
	/**
	 * The db BEGIN statement should only be called one time because the
	 * implementation is virtual ... only the outermost begin/commit/rollback
	 * issue commands.
	 *
	 * @covers ::start
	 * @covers ::doStart
	 */
	public function testStart()
	{
		$db = $this->getMock(DriverInterface::class);

		$db->expects($this->once())->method('execute')->with('BEGIN');

		$transaction = new EmulatedNestedTransactionDriver($db);

		$transaction->start();
		$transaction->start();
		$transaction->start();
	}

	/**
	 * @covers ::commit
	 * @covers ::doCommit
	 */
	public function testCommit()
	{
		$db = $this->getMock(DriverInterface::class);

		$transaction = new EmulatedNestedTransactionDriver($db);

		$transaction->start();
		$transaction->start();
		$transaction->start();

		$db->expects($this->once())->method('execute')->with('COMMIT');

		$transaction->commit();
		$transaction->commit();
		$transaction->commit();
	}

	/**
	 * @covers ::commit
	 * @covers ::doCommit
	 */
	public function testNotReadyToCommit()
	{
		$db = $this->getMock(DriverInterface::class);

		$transaction = new EmulatedNestedTransactionDriver($db);

		$transaction->start();
		$transaction->start();

		$db->expects($this->never())->method('execute')->with('COMMIT');

		$transaction->commit();
	}

	/**
	 * @covers ::commit
	 */
	public function testCommitWithoutTransaction()
	{
		$this->expectException(TransactionNotStartedException::class);
		$db = $this->getMock(DriverInterface::class);

		$transaction = new EmulatedNestedTransactionDriver($db);
		$transaction->commit();
	}

	/**
	 * @covers ::commit
	 */
	public function testCommitWithRolledBackInnerTransaction()
	{
		$this->expectException(CommitException::class);

		$db = $this->getMock(DriverInterface::class);

		$transaction = new EmulatedNestedTransactionDriver($db);
		$transaction->start();
		$transaction->start();
		$transaction->rollBack();
		$transaction->commit();
	}

	/**
	 * @covers ::rollBack
	 * @covers ::doRollBack
	 */
	public function testRollBack()
	{
		$db = $this->getMock(DriverInterface::class);

		$transaction = new EmulatedNestedTransactionDriver($db);

		$transaction->start();
		$transaction->start();
		$transaction->start();

		$db->expects($this->once())->method('execute')->with('ROLLBACK');

		$transaction->rollBack();
		$transaction->rollBack();
		$transaction->rollBack();
	}

	/**
	 * @covers ::rollBack
	 */
	public function testRollBackWithoutTransaction()
	{
		$this->expectException(TransactionNotStartedException::class);

		$db = $this->getMock(DriverInterface::class);

		$transaction = new EmulatedNestedTransactionDriver($db);
		$transaction->rollBack();
	}

	/**
	 * @covers ::rollBack
	 */
	public function testRollBackFollowedByGoodTransaction()
	{
		$db = $this->getMock(DriverInterface::class);

		$transaction = new EmulatedNestedTransactionDriver($db);

		$transaction->start();
		$transaction->start();
		$transaction->rollBack();
		$transaction->rollBack();

		// The internal rollback pointer should be reset now, so the following
		// transaction succeeds.

		$transaction->start();

		$db->expects($this->once())->method('execute')->with('COMMIT');

		$transaction->commit();
	}

	/**
	 * Test that a transaction exists.
	 *
	 * @covers ::exists
	 */
	public function testExists()
	{
		$db = $this->getMock(DriverInterface::class);

		$db->expects($this->exactly(2))->
			method('execute')->
			withConsecutive(['BEGIN'], ['COMMIT']);

		$transaction = new EmulatedNestedTransactionDriver($db);

		$this->assertFalse($transaction->exists());
		$transaction->start();
		$this->assertTrue($transaction->exists());
		$transaction->commit();
		$this->assertFalse($transaction->exists());
	}
}

