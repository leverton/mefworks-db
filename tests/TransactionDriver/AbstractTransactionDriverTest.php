<?php
use mef\Db\Driver\DriverInterface;
use mef\Db\TransactionDriver\AbstractTransactionDriver;

/**
 * @coversDefaultClass mef\Db\TransactionDriver\AbstractTransactionDriver
 */
class AbstractTransactionDriverTest extends \mef\Db\Test\AbstractTest
{
	/**
	 * @covers ::__construct
	 * @covers ::getDatabaseDriver
	 * @covers ::setDatabaseDriver
	 */
	public function testAccessors()
	{
		$db = $this->getMock(DriverInterface::class);

		$transaction = $this->getMockBuilder(AbstractTransactionDriver::class)->
			setMethods(['start', 'commit', 'rollBack', 'exists'])->
			setConstructorArgs([$db])->
			getMock();

		$this->assertSame($db, $transaction->getDatabaseDriver());

		$db = $this->getMock(DriverInterface::class);
		$this->assertNotSame($db, $transaction->getDatabaseDriver());

		$transaction->setDatabaseDriver($db);
		$this->assertSame($db, $transaction->getDatabaseDriver());
	}
}

