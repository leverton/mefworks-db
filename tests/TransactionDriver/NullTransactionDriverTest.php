<?php

use mef\Db\TransactionDriver\Exception\TransactionNotStartedException;
use mef\Db\TransactionDriver\NullTransactionDriver;

/**
 * @coversDefaultClass mef\Db\TransactionDriver\NullTransactionDriver
 */
class NullTransactionDriverTest extends \mef\Db\Test\AbstractTest
{
	public function setUp(): void
	{
		$this->transactionDriver = new NullTransactionDriver;
	}

	/**
	 * @covers ::start
	 */
	public function testStart()
	{
		$this->transactionDriver->start();
		$this->assertSame(1, $this->transactionDriver->getDepth());
	}

	/**
	 * @covers ::commit
	 */
	public function testCommit()
	{
		$this->transactionDriver->start();
		$this->transactionDriver->commit();
		$this->assertSame(0, $this->transactionDriver->getDepth());
	}

	/**
	 * @covers ::commit
	 */
	public function testCommitWithNoTransaction()
	{
		$this->expectException(TransactionNotStartedException::class);

		$this->transactionDriver->commit();
	}

	/**
	 * @covers ::rollBack
	 */
	public function testRollBack()
	{
		$this->transactionDriver->start();
		$this->transactionDriver->rollBack();
		$this->assertSame(0, $this->transactionDriver->getDepth());
	}

	/**
	 * @covers ::rollBack
	 */
	public function testRollBackWithNoTransaction()
	{
		$this->expectException(TransactionNotStartedException::class);

		$this->transactionDriver->rollBack();
	}

	/**
	 * @covers ::getDepth
	 */
	public function testGetDepth()
	{
		$this->assertSame(0, $this->transactionDriver->getDepth());

		$this->transactionDriver->start();
		$this->assertSame(1, $this->transactionDriver->getDepth());

		$this->transactionDriver->start();
		$this->assertSame(2, $this->transactionDriver->getDepth());

		$this->transactionDriver->rollBack();
		$this->assertSame(1, $this->transactionDriver->getDepth());

		$this->transactionDriver->commit();
		$this->assertSame(0, $this->transactionDriver->getDepth());
	}

	/**
	 * Test that a transaction exists.
	 *
	 * @covers ::exists
	 */
	public function testExists()
	{
		$this->assertFalse($this->transactionDriver->exists());
		$this->transactionDriver->start();
		$this->assertTrue($this->transactionDriver->exists());
		$this->transactionDriver->commit();
		$this->assertFalse($this->transactionDriver->exists());
	}
}