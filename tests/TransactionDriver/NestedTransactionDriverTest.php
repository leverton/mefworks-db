<?php

use mef\Db\Driver\DriverInterface;
use mef\Db\Driver\Exception\ExecuteException;
use mef\Db\TransactionDriver\Exception\TransactionNotStartedException;
use mef\Db\TransactionDriver\NestedTransactionDriver;

/**
 * @coversDefaultClass mef\Db\TransactionDriver\NestedTransactionDriver
 */
class NestedTransactionDriverTest extends \mef\Db\Test\AbstractTest
{
	public function setUp(): void
	{
		$this->db = $this->getMock(DriverInterface::class);
		$this->transaction = new NestedTransactionDriver($this->db);
	}

	/**
	 * @covers ::start
	 * @covers ::getStartSyntax
	 */
	public function testStart()
	{
		$this->db->expects($this->once())->method('execute')->with('BEGIN');

		$this->transaction->start();
	}

	/**
	 * @covers ::start
	 * @covers ::getSavePointSyntax
	 */
	public function testSavePoint()
	{
		$this->transaction->start();

		$this->db->expects($this->once())->method('execute')->with('SAVEPOINT _sp_1');
		$this->transaction->start();
	}

	/**
	 * @covers ::commit
	 * @covers ::getCommitSyntax
	 * @covers ::getReleaseSavePointSyntax
	 */
	public function testCommit()
	{
		$this->transaction->start();
		$this->transaction->start();

		$this->db->expects($this->exactly(2))->
			method('execute')->
			withConsecutive(['RELEASE SAVEPOINT _sp_1'], ['COMMIT']);

		$this->transaction->commit();
		$this->transaction->commit();
	}

	/**
	 * @covers ::commit
	 */
	public function testCommitWithoutTransaction()
	{
		$this->expectException(TransactionNotStartedException::class);

		$this->transaction->commit();
	}

	/**
	 * @covers ::commit
	 * @covers ::popStack
	 */
	public function testCommitWithException()
	{
		$this->expectException(TransactionNotStartedException::class);

		$matcher = $this->exactly(3);

		$this->db->expects($matcher)->
			method('execute')->
			withConsecutive(['BEGIN'], ['COMMIT'], ['ROLLBACK'])->
			willReturnCallback(function() use ($matcher) {
				if ($matcher->getInvocationCount() == 2) {
					throw new ExecuteException('COMMIT');
				}

				return 0;
			});

		$this->transaction->start();
		$this->transaction->commit();
	}

	/**
	 * @covers ::rollBack
	 * @covers ::getRollBackSyntax
	 * @covers ::getRollBackSavePointSyntax
	 * @covers ::popStack
	 */
	public function testRollBack()
	{
		$this->transaction->start();
		$this->transaction->start();

		$this->db->expects($this->exactly(2))->
			method('execute')->
			withConsecutive(['ROLLBACK TO SAVEPOINT _sp_1'], ['ROLLBACK']);

		$this->transaction->rollBack();
		$this->transaction->rollBack();
	}

	/**
	 * @covers ::rollBack
	 * @covers ::popStack
	 */
	public function testRollBackWithoutTransaction()
	{
		$this->expectException(TransactionNotStartedException::class);
		$this->transaction->rollBack();
	}

	/**
	 * @covers ::rollBack
	 * @covers ::popStack
	 */
	public function testRollBackException()
	{
		$this->expectException(TransactionNotStartedException::class);

		$matcher = $this->exactly(3);

		$this->db->expects($matcher)->
			method('execute')->
			withConsecutive(['BEGIN'], ['ROLLBACK'], ['ROLLBACK'])->
			willReturnCallback(function() use ($matcher) {
				if ($matcher->getInvocationCount() == 2) {
					throw new ExecuteException('ROLLBACK');
				}

				return 0;
			});

		$this->transaction->start();
		$this->transaction->rollBack();
	}

	/**
	 * @covers ::getDepth
	 */
	public function testGetDepth()
	{
		$this->assertSame(0, $this->transaction->getDepth());

		$this->transaction->start();
		$this->assertSame(1, $this->transaction->getDepth());

		$this->transaction->start();
		$this->assertSame(2, $this->transaction->getDepth());

		$this->transaction->start();
		$this->assertSame(3, $this->transaction->getDepth());

		$this->transaction->rollBack();
		$this->assertSame(2, $this->transaction->getDepth());

		$this->transaction->rollBack();
		$this->assertSame(1, $this->transaction->getDepth());

		$this->transaction->commit();
		$this->assertSame(0, $this->transaction->getDepth());
	}

	/**
	 * Test that a transaction exists.
	 *
	 * @covers ::exists
	 */
	public function testExists()
	{
		$this->db->expects($this->exactly(2))->
			method('execute')->
			withConsecutive(['BEGIN'], ['COMMIT']);

		$this->assertFalse($this->transaction->exists());
		$this->transaction->start();
		$this->assertTrue($this->transaction->exists());
		$this->transaction->commit();
		$this->assertFalse($this->transaction->exists());
	}}