<?php
use mef\Db\Driver\DriverInterface;
use mef\Db\Driver\PdoDriver;
use mef\Db\TransactionDriver\PdoEmulatedNestedTransactionDriver;

/**
 * @coversDefaultClass mef\Db\TransactionDriver\PdoEmulatedNestedTransactionDriver
 */
class PDOEmulatedNestedTransactionDriverTest extends \mef\Db\Test\AbstractTest
{
	/**
	 * @covers ::setDatabaseDriver
	 */
	public function testBadDriver()
	{
		$this->expectException(InvalidArgumentException::class);
		$db = $this->getMockBuilder(DriverInterface::class)->getMock();
		new PdoEmulatedNestedTransactionDriver($db);
	}

	/**
	 * @covers ::doStart
	 * @covers ::setDatabaseDriver
	 */
	public function testStart()
	{
		$pdo = $this->getMockBuilder('PDO')->disableOriginalConstructor()->getMock();
		$pdo->method('getAttribute')->will($this->returnValue(PDO::ERRMODE_EXCEPTION));

		$db = $this->getMockBuilder(PdoDriver::class)->setConstructorArgs([$pdo])->getMock();
		$pdo->expects($this->once())->method('beginTransaction');

		$transaction = new PdoEmulatedNestedTransactionDriver($db);
		$transaction->start();
	}

	/**
	 * @covers ::doCommit
	 */
	public function testCommit()
	{
		$pdo = $this->getMockBuilder('PDO')->disableOriginalConstructor()->getMock();
		$pdo->method('getAttribute')->will($this->returnValue(PDO::ERRMODE_EXCEPTION));

		$db = $this->getMockBuilder(PdoDriver::class)->setConstructorArgs([$pdo])->getMock();
		$pdo->expects($this->once())->method('commit');

		$transaction = new PdoEmulatedNestedTransactionDriver($db);
		$transaction->start();
		$transaction->commit();
	}

	/**
	 * @covers ::doRollBack
	 */
	public function testRollBack()
	{
		$pdo = $this->getMockBuilder('PDO')->disableOriginalConstructor()->getMock();
		$pdo->method('getAttribute')->will($this->returnValue(PDO::ERRMODE_EXCEPTION));

		$db = $this->getMockBuilder(PdoDriver::class)->setConstructorArgs([$pdo])->getMock();
		$pdo->expects($this->once())->method('rollBack');

		$transaction = new PdoEmulatedNestedTransactionDriver($db);
		$transaction->start();
		$transaction->rollBack();
	}
}

