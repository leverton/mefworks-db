<?php

use mef\Db\Driver\Exception\ExecuteException;
use mef\Db\Driver\Exception\PrepareException;
use mef\Db\Driver\Exception\QueryException;
use mef\Db\Driver\MySqliDriver;
use mef\Db\RecordSet\MySqliRecordSet;
use mef\Db\Statement\MySqliStatement;

/**
 * @coversDefaultClass mef\Db\Driver\MySqliDriver
 */
class MySqliDriverTest extends \mef\Db\Test\AbstractTest
{
	public function setUp(): void
	{
		$this->mysqli = $this->getMockBuilder('mysqli')->
			getMock();

		$this->driver = new MySqliDriver($this->mysqli);
	}

	/**
	 * @covers ::__construct
	 * @covers ::getMySqli
	 */
	public function testAccessors()
	{
		$this->assertSame($this->mysqli, $this->driver->getMySqli());
	}

	/**
	 * @covers ::query
	 */
	public function testQuery()
	{
		$sql = 'SELECT 1';

		$rs = $this->getMockBuilder('mysqli_result')->
			disableOriginalConstructor()->
			getMock();

		$this->mysqli->expects($this->once())->method('real_query')->with($sql);
		$this->mysqli->expects($this->once())->method('store_result')->will($this->returnValue($rs));

		$this->assertTrue($this->driver->query($sql) instanceof MySqliRecordSet);
	}

	/**
	 * @covers ::query
	 */
	public function testQueryWithFailedRealQuery()
	{
		$this->expectException(QueryException::class);
		$sql = 'SELECT 1';

		$rs = $this->getMockBuilder('mysqli_result')->
			disableOriginalConstructor()->
			getMock();

		$this->mysqli->expects($this->once())->
			method('real_query')->
			with($sql)->
			will($this->returnValue(false));

		$this->mysqli->expects($this->never())->method('store_result');

		$this->driver->query($sql);
	}

	/**
	 * @covers ::execute
	 */
	public function testExecute()
	{
		$sql = 'SELECT 1';

		$this->mysqli->expects($this->once())->method('real_query')->with($sql);
		$this->mysqli->expects($this->once())->method('store_result');

		@$this->driver->execute($sql);
	}

	/**
	 * @covers ::execute
	 */
	public function testExecuteWithFailedRealQuery()
	{
		$this->markTestIncomplete("This doesn't work because the mysqli->error field won't be set and cannot be mocked.");
		$this->expectException(ExecuteException::class);

		$sql = 'SELECT 1';

		$this->mysqli->expects($this->once())->
			method('real_query')->
			with($sql)->
			willReturn(false);

		$this->mysqli->expects($this->never())->method('store_result');
		@$this->driver->execute($sql);
	}

	/**
	 * @covers ::execute
	 */
	public function testExecuteWithFailedStoreResult()
	{
		$this->markTestIncomplete("This doesn't work because the mysqli->error field won't be set and cannot be mocked.");
		$this->expectException(ExecuteException::class);

		$sql = 'SELECT 1';

		$this->mysqli->expects($this->once())->
			method('real_query')->
			with($sql)->
			willReturn(true);

		$this->mysqli->expects($this->once())->
			method('store_result')->
			willReturn(false);

		$this->driver->execute($sql);
	}
	/**
	 * @covers ::prepare
	 */
	public function testPrepare()
	{
		$sql = 'SELECT';

		$mysqliStmt = $this->getMockBuilder('mysqli_stmt')->
			disableOriginalConstructor()->
			getMock();

		$this->mysqli->expects($this->once())->method('prepare')->with($sql)->willReturn($mysqliStmt);
		$this->assertTrue($this->driver->prepare($sql) instanceof MySqliStatement);
	}

	/**
	 * @covers ::prepare
	 */
	public function testPrepareWithNamedParameters()
	{
		$sql = 'SELECT * FROM t WHERE a=:a AND b=:b';

		$mysqliStmt = $this->getMockBuilder('mysqli_stmt')->
			disableOriginalConstructor()->
			getMock();

		$this->mysqli->expects($this->once())->method('prepare')->with( 'SELECT * FROM t WHERE a=? AND b=?')->willReturn($mysqliStmt);
		$st = $this->driver->prepare($sql);
		$this->assertTrue($st instanceof MySqliStatement);
		$this->assertEquals([':a' => [0], ':b' => [1]], $st->getNamedParameters());
	}

	/**
	 * @covers ::prepare
	 */
	public function testPrepareFailure()
	{
		$this->expectException(PrepareException::class);

		$sql = 'SELECT';

		$this->mysqli->expects($this->once())->method('prepare')->with($sql)->willReturn(false);
		$this->driver->prepare($sql);
	}

	/**
	 * @covers ::quoteValue
	 */
	public function testQuoteValue()
	{
		$this->mysqli->expects($this->once())->method('real_escape_string')->with('foo')->will($this->returnValue('bar'));

		$this->assertSame('bar', $this->driver->quoteValue('foo'));
	}
}