<?php

use mef\Db\Driver\Exception\ExecuteException;

/**
 * @coversDefaultClass mef\Db\Driver\Exception\ExecuteException
 */
class ExecuteExceptionTest extends \mef\Db\Test\AbstractTest
{
	/**
	 * @covers ::__construct
	 * @covers ::getSql
	 * @covers ::getMessage
	 */
	public function testAccessors()
	{
		$sql = 'SELECT';
		$driverMessage = 'error';

		$exception = new ExecuteException($sql, $driverMessage);

		$this->assertSame($sql, $exception->getSql());
		$this->assertSame($driverMessage, $exception->getMessage());
	}

	/**
	 * @covers ::fromException
	 */
	public function testFromException()
	{
		$previousException = new Exception("Message", 42);

		$exception = ExecuteException::fromException($previousException, 'sql');

		$this->assertSame($previousException, $exception->getPrevious());
	}
}