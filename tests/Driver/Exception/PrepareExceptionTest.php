<?php

use mef\Db\Driver\Exception\PrepareException;

/**
 * @coversDefaultClass mef\Db\Driver\Exception\PrepareException
 */
class PrepareExceptionTest extends \mef\Db\Test\AbstractTest
{
	/**
	 * @covers ::__construct
	 * @covers ::getSql
	 * @covers ::getMessage
	 */
	public function testAccessors()
	{
		$sql = 'SELECT';
		$driverMessage = 'error';

		$exception = new PrepareException($sql, $driverMessage);

		$this->assertSame($sql, $exception->getSql());
		$this->assertSame($driverMessage, $exception->getMessage());
	}
}