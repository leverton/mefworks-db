<?php

use mef\Db\Driver\Exception\AbstractSqlException;

/**
 * @coversDefaultClass mef\Db\Driver\Exception\AbstractSqlException
 */
class AbstractExceptionTest extends \mef\Db\Test\AbstractTest
{
	/**
	 * @covers ::__construct
	 * @covers ::getSql
	 * @covers ::getMessage
	 */
	public function testAccessors()
	{
		$sql = 'SELECT';
		$driverMessage = 'error';

		$exception = $this->getMockForAbstractClass(
			AbstractSqlException::class,
			[$sql, $driverMessage]
		);

		$this->assertSame($sql, $exception->getSql());
		$this->assertSame($driverMessage, $exception->getMessage());
	}
}