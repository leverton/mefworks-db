<?php

use mef\Db\Driver\DataProvider\ArrayDataProvider;

/**
 * @coversDefaultClass mef\Db\Driver\DataProvider\ArrayDataProvider
 */
class ArrayDataProviderTest extends \mef\Db\Test\AbstractTest
{
	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$this->assertTrue(new ArrayDataProvider([]) instanceof ArrayDataProvider);
	}
}