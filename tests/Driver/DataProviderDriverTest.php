<?php

use mef\Db\Driver\DataProviderDriver;
use mef\Db\Driver\DataProvider\DataProviderInterface;
use mef\Db\RecordSet\RecordSetInterface;
use mef\Db\Statement\StatementInterface;

/**
 * @coversDefaultClass mef\Db\Driver\DataProviderDriver
 */
class DataProviderDriverTest extends \mef\Db\Test\AbstractTest
{
	public function setUp(): void
	{
		$this->dataProvider = $this->getMock(DataProviderInterface::class);
		$this->driver = new DataProviderDriver($this->dataProvider);
	}

	/**
	 * @covers ::__construct
	 * @covers ::getDataProvider
	 */
	public function testAccessors()
	{
		$this->assertSame($this->dataProvider, $this->driver->getDataProvider());
	}

	/**
	 * @covers ::query
	 */
	public function testQuery()
	{
		$sql = 'SELECT';
		$iterator = new ArrayIterator([]);

		$this->dataProvider->expects($this->once())->
			method('getDataForQuery')->
			with($sql)->
			will($this->returnValue($iterator));

		$this->assertTrue($this->driver->query($sql) instanceof RecordSetInterface);
	}

	/**
	 * @covers ::execute
	 */
	public function testExecute()
	{
		$sql = 'UPDATE';
		$returnValue = 42;

		$this->dataProvider->expects($this->once())->
			method('executeQuery')->
			with($sql)->
			will($this->returnValue($returnValue));

		$this->assertSame($returnValue, $this->driver->execute($sql));
	}

	/**
	 * @covers ::prepare
	 */
	public function testPrepare()
	{
		$sql = 'UPDATE';
		$parameters = [];

		$this->assertTrue($this->driver->prepare($sql, $parameters) instanceof StatementInterface);
	}

	/**
	 * @covers ::quoteValue
	 */
	public function testQuoteValue()
	{
		$this->assertTrue(is_string($this->driver->quoteValue('42')));
	}
}