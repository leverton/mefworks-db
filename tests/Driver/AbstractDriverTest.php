<?php

use mef\Db\Driver\AbstractDriver;
use mef\Db\RecordSet\AbstractRecordSet;
use mef\Db\TransactionDriver\TransactionDriverInterface;

/**
 * @coversDefaultClass mef\Db\Driver\AbstractDriver
 */
class AbstractDriverTest extends \mef\Db\Test\AbstractTest
{
	public function setUp(): void
	{
		$this->driver = new Driver;
	}

	/**
	 * @covers ::getTransactionDriver
	 * @covers ::setTransactionDriver
	 */
	public function testTransactionDriverAccessors()
	{
		$this->assertNull($this->driver->getTransactionDriver());

		$engine = $this->getMock(TransactionDriverInterface::class);
		$this->driver->setTransactionDriver($engine);

		$this->assertSame($engine, $this->driver->getTransactionDriver());
	}

	/**
	 * @covers ::startTransaction
	 * @covers ::inTransaction
	 */
	public function testStartTransaction()
	{
		$engine = $this->getMock(TransactionDriverInterface::class);
		$engine->expects($this->exactly(1))->method('start');
		$engine->expects($this->exactly(2))->method('exists')->will(
			$this->onConsecutiveCalls(false, true)
		);

		$this->driver->setTransactionDriver($engine);
		$this->assertFalse($this->driver->inTransaction());
		$this->driver->startTransaction();
		$this->assertTrue($this->driver->inTransaction());
	}

	/**
	 * @covers ::commit
	 */
	public function testCommit()
	{
		$engine = $this->getMock(TransactionDriverInterface::class);
		$engine->expects($this->exactly(1))->method('commit');

		$this->driver->setTransactionDriver($engine);
		$this->driver->commit();
	}

	/**
	 * @covers ::rollBack
	 */
	public function testRollback()
	{
		$engine = $this->getMock(TransactionDriverInterface::class);
		$engine->expects($this->exactly(1))->method('rollBack');

		$this->driver->setTransactionDriver($engine);
		$this->driver->rollBack();
	}

	/**
	 * @covers ::startTransaction
	 */
	public function testStartTransactionWithNoDriver()
	{
		$this->expectException(RuntimeException::class);
		$driver = $this->getMockForAbstractClass(AbstractDriver::class);
		$driver->startTransaction();
	}

	/**
	 * @covers ::commit
	 */
	public function testCommitWithNoDriver()
	{
		$this->expectException(RuntimeException::class);
		$driver = $this->getMockForAbstractClass(AbstractDriver::class);
		$driver->commit();
	}

	/**
	 * @covers ::rollBack
	 */
	public function testRollBackWithNoDriver()
	{
		$this->expectException(RuntimeException::class);
		$driver = $this->getMockForAbstractClass(AbstractDriver::class);
		$driver->rollBack();
	}
}

class Driver extends AbstractDriver
{
	public function query($sql)
	{
		if ($sql === '')
		{
			$data = [
				['k' => '1', 'v' => 'one'],
				['k' => '2', 'v' => 'two'],
			];
		}
		else
		{
			$data = [
				['k' => '1', 'v' => 'one', 'e' => null],
				['k' => '2', 'v' => 'two', 'e' => null],
			];
		}

		return new RecordSet($data);
	}

	public function execute($sql)
	{
		return 0;
	}

	public function prepare($sql, array $params = [])
	{
		throw new Exception('Not Implemented');
	}

	public function quoteValue($value)
	{
		return $value;
	}
}

class RecordSet extends AbstractRecordSet
{
	protected $iterator;

	public function __construct(array $data)
	{
		$this->iterator = new ArrayIterator($data);
	}

	public function close()
	{
		$this->iterator = null;
	}

	public function count() : int
	{
		return 2;
	}

	public function fetchRow()
	{
		$row = $this->iterator->current();
		$this->iterator->next();
		return $row ?: [];
	}

	public function fetchRowAsArray()
	{
		$row = $this->iterator->current();
		$this->iterator->next();
		return array_values($row ?: []);
	}

	public function getIterator() : Traversable
	{
		return $this->iterator;
	}
}