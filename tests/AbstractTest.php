<?php
namespace mef\Db\Test;

use PHPUnit\Framework\TestCase;

abstract class AbstractTest extends TestCase
{
    public function getMock(string $className)
    {
        return $this->getMockBuilder($className)->getMock();
    }
}