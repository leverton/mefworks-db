<?php

use mef\Db\RecordSet\PDORecordSet;

/**
 * @coversDefaultClass mef\Db\RecordSet\PDORecordSet
 */
class PDORecordSetTest extends \mef\Db\Test\AbstractTest
{
	public function setUp(): void
	{
		$this->statement = $this->getMock('PDOStatement');
		$this->rs = new PDORecordSet($this->statement);
	}

	/**
	 * @covers ::__construct
	 * @covers ::getPdoStatement
	 */
	public function testAccessors()
	{
		$this->assertSame($this->statement, $this->rs->getPdoStatement());
	}

	/**
	 * @covers ::close
	 */
	public function testClose()
	{
		$this->statement->expects($this->once())->method('closeCursor');

		$this->rs->close();
		$this->assertNull($this->rs->getPdoStatement());
	}

	/**
	 * @covers ::count
	 */
	public function testCount()
	{
		$this->statement->expects($this->exactly(2))->
			method('rowCount')->
			will($this->returnValue(42));

		$this->assertSame(42, $this->rs->count());
		$this->assertSame(42, count($this->rs));
	}

	/**
	 * @covers ::fetchRow
	 */
	public function testFetchRow()
	{
		$data = ['key' => 'val'];

		$this->statement->expects($this->exactly(2))->
			method('fetch')->
			with(PDO::FETCH_ASSOC)->
			will($this->onConsecutiveCalls($data, null));

		$this->assertSame($data, $this->rs->fetchRow());
		$this->assertSame([], $this->rs->fetchRow());
	}

	/**
	 * @covers ::fetchRowAsArray
	 */
	public function testFetchRowAsArray()
	{
		$data = [0 => 'val'];

		$this->statement->expects($this->exactly(2))->
			method('fetch')->
			with(PDO::FETCH_NUM)->
			will($this->onConsecutiveCalls($data, null));

		$this->assertSame($data, $this->rs->fetchRowAsArray());
		$this->assertSame([], $this->rs->fetchRowAsArray());
	}
}