<?php

use mef\Db\RecordSet\ArrayRecordSet;

/**
 * @coversDefaultClass mef\Db\RecordSet\ArrayRecordSet
 */
class ArrayRecordSetTest extends \mef\Db\Test\AbstractTest
{
	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$rs = new ArrayRecordSet(['foo']);

		$this->assertSame(1, count($rs));
	}
}