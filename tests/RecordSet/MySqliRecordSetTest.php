<?php

use mef\Db\RecordSet\MySqliRecordSet;

class MysqliResultMock extends mysqli_result
{
	private $data;
	private $i;

	public function __construct(array $data = [])
	{
		$this->data = $data;
		$this->i = 0;
	}

	public function getFoo()
	{
		return 42;
	}

	public function count()
	{
		return count($this->data);
	}

	public function fetch_assoc() : array
	{
		if ($this->i < count($this->data))
		{
			return $this->data[$this->i++];
		}
		else
		{
			return [];
		}
	}

	public function fetch_row() : array
	{
		if ($this->i < count($this->data))
		{
			return array_values($this->data[$this->i++]);
		}
		else
		{
			return [];
		}
	}

	public function free()
	{

	}
}

/**
 * @coversDefaultClass mef\Db\RecordSet\MySqliRecordSet
 */
class MySqliRecordSetTest extends \mef\Db\Test\AbstractTest
{
	public function getRecordSet(array $data = [])
	{
		#$this->result = $this->getMockBuilder('mysqli_result')->
		#	disableOriginalConstructor()->
		#	getMock();

		$result = $this->getMockBuilder(MysqliResultMock::class)->
			setConstructorArgs([$data])->
			setMethods(['free'])->
			getMock();

		return new MySqliRecordSet($result);
	}

	/**
	 * @covers ::__construct
	 * @covers ::getMySqliResult
	 */
	public function testAccessors()
	{
		$result = $this->getMockBuilder(MysqliResultMock::class)->
			setMethods(['free'])->
			getMock();

		$rs = new MySqliRecordSet($result);

		$this->assertSame($result, $rs->getMySqliResult());
	}

	/**
	 * @covers ::close
	 */
	public function testClose()
	{
		$result = $this->getMockBuilder(MysqliResultMock::class)->
			setMethods(['free'])->
			getMock();

		$rs = new MySqliRecordSet($result);

		$result->expects($this->once())->method('free');

		$rs->close();
		$this->assertNull($rs->getMySqliResult());
	}

	/**
	 * @covers ::count
	 */
	public function testCount()
	{
		$rs = $this->getRecordSet();
		// MySQLi will throw a warning here because we haven't actually ran a
		// query.
		@$this->assertSame(0, $rs->count());
		@$this->assertSame(0, count($rs));
	}

	/**
	 * @covers ::fetchRow
	 */
	public function testFetchRow()
	{
		$data = ['key' => 'val'];
		$rs = $this->getRecordSet([$data]);

		$this->assertSame($data, $rs->fetchRow());
		$this->assertSame([], $rs->fetchRow());
	}

	/**
	 * @covers ::fetchRowAsArray
	 */
	public function testFetchRowAsArray()
	{
		$data = ['key' => 'val'];
		$rs = $this->getRecordSet([$data]);

		$this->assertSame(array_values($data), $rs->fetchRowAsArray());
		$this->assertSame([], $rs->fetchRowAsArray());
	}
}