<?php

use mef\Db\Statement\AbstractStatement;

/**
 * @coversDefaultClass mef\Db\Statement\AbstractStatement
 */

class AbstractStatementTest extends \mef\Db\Test\AbstractTest
{
	/**
	 * @covers ::setParameters
	 */
	public function testIndexedParameters()
	{
		$st = $this->getMockForAbstractClass(AbstractStatement::class);

		$st->expects($this->exactly(3))->method('setParameter');

		$st->setParameters([1, 2, 3]);
	}

	/**
	 * @covers ::setParameters
	 */
	public function testNamedParameters()
	{
		$st = $this->getMockForAbstractClass(AbstractStatement::class);

		$st->expects($this->exactly(3))->method('setParameter');

		$st->setParameters([
			':a' => '1',
			':b' => '2',
			':c' => '3',
		]);
	}

	/**
	 * @covers ::bindParameters
	 */
	public function testBoundIndexedParameters()
	{
		$st = $this->getMockForAbstractClass(AbstractStatement::class);

		$st->expects($this->exactly(3))->method('bindParameter');

		$st->bindParameters([1, 2, 3]);
	}

	/**
	 * @covers ::bindParameters
	 */
	public function testBoundNamedParameters()
	{
		$st = $this->getMockForAbstractClass(AbstractStatement::class);

		$st->expects($this->exactly(3))->method('bindParameter');

		$st->bindParameters([
			':a' => '1',
			':b' => '2',
			':c' => '3',
		]);
	}
}