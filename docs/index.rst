=======
mef\\Db
=======

mef\\Db is a database driver with a simple and consistent object oriented
interface that currently works with both PDO and mysqli extensions. It supports
prepared (named and indexed) statements and nested transactions.

Results can be iterated over using an iterator or returned as an array. Each
row can be returned as an associative array, an indexed array, an object, or
anything mapped by a callback function.

User guide
----------

.. toctree::
    :maxdepth: 2

    overview
    quickstart
    drivers
    statements
    recordsets
    transactions

.. todolist::