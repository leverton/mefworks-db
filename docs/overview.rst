========
Overview
========

Requirements
============

* PHP 5.6.0
* mysqli or PDO extension

Example
=======

.. code-block:: php

    $driver = new mef\Db\Driver\PdoDriver(new PDO('sqlite::memory:'));
    $driver->execute('CREATE TABLE test (
        "id" INTEGER PRIMARY KEY,
        "key" TEXT, "value" TEXT
    )');

    $driver->prepare(
        'INSERT INTO test VALUES (:id, :key)',
        [':id' => 1, ':key' => 'apple']
    )->execute();

    foreach ($driver->query('SELECT * FROM test') as $row)
    {
        echo $row['id'], ' => ', $row['key'], PHP_EOL;
    }

More complete examples are available in the ``examples`` directory.

Installation
============

The recommended way to install mef\\Db is with `Composer <http://getcomposer.org>`_.

.. code-block:: bash

    php composer.phar require mefworks/db:~1.0

Alternatively, you can download the source from
`BitBucket <https://bitbucket.org/leverton/mefworks-db>`_. It is compatible with
`PSR-4 <http://www.php-fig.org/psr/psr-4/>`_ class loaders.

.. code-block:: bash

    git clone git@bitbucket.org:leverton/mefworks-db.git

License
=======

Licensed using the `MIT license <http://opensource.org/licenses/MIT>`_.

    Copyright (C) 2006-2014 Matthew Leverton

    Permission is hereby granted, free of charge, to any person obtaining a copy of
    this software and associated documentation files (the "Software"), to deal in
    the Software without restriction, including without limitation the rights to
    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
    of the Software, and to permit persons to whom the Software is furnished to do
    so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.