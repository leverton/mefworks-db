<?php namespace mef\Db\RecordSet;

use Traversable;

interface RecordSetInterface
{
	/**
	 * Return the number of records.
	 *
	 * @return integer
	 */
	public function count(): int;

	/**
	 * Close the record set to free up resources.
	 *
	 * It is no longer usable.
	 */
	public function close();

	/**
	 * Fetch the next row and return a single value from it.
	 *
	 * By default the first column is used.
	 *
	 * Return null if no rows exist.
	 *
	 * @param integer $index  0-based column to use
	 *
	 * @return mixed
	 */
	public function fetchValue($index = 0);

	/**
	 * Fetch the next row and return as an associative array.
	 *
	 * Return an empty array if no more rows exist.
	 *
	 * @return array
	 */
	public function fetchRow();

	/**
	 * Fetch the next row and return as an indexed (0-based) array.
	 *
	 * Return an empty array if no more rows exist.
	 *
	 * @return array
	 */
	public function fetchRowAsArray();

	/**
	 * Fetch the next row and replace the contents of the specificed
	 * associative array with it.
	 *
	 * Keys in the supplied array that do not exist in the row will
	 * not be modified.
	 *
	 * @param  array  &$array
	 *
	 * @return boolean  true if the array was updated
	 *                  false if there are no more rows
	 */
	public function fetchRowInto(array &$array);

	/**
	 * Fetch the next row and replace the contents of the specificed
	 * indexed array with it.
	 *
	 * Indexes in the supplied array that do not exist in the row will
	 * not be modified.
	 *
	 * @param  array  &$array
	 *
	 * @return boolean  true if the array was updated
	 *                  false if there are no more rows
	 */
	public function fetchRowIntoArray(array &$array);

	/**
	 * Fetch the next row and replace the contents of the specificed
	 * object with it (by treating the name of the column as the name of
	 * an object property.)
	 *
	 * Properties in the supplied object that do not exist in the row will
	 * not be modified.
	 *
	 * @param  array  &$array
	 *
	 * @return boolean  true if the array was updated
	 *                  false if there are no more rows
	 */
	public function fetchRowIntoObject(&$object);

	/**
	 * Return the entire resultset as an indexed array of associative rows.
	 *
	 * @return array       An indexed array of associative rows
	 */
	public function fetchAll();

	/**
	 * Return the entire resultset as an associative array of associative
	 * rows.
	 *
	 * The specified key will be omitted from the row. If no key is supplied,
	 * then the first column is used.
	 *
	 * @param string $key  The column to use as the key.
	 *
	 * @return array       An associative array of associative rows
	 */
	public function fetchKeyed($key = '');

	/**
	 * Return the entire resultset as an indexed array of indexed rows.
	 *
	 * @return array       An indexed array of indexed rows
	 */
	public function fetchAllAsArray();

	/**
	 * Return the entire resultset as an indexed array of values.
	 *
	 * @param integer $index  The 0-based column index to return
	 *
	 * @return array       An indexed array of indexed rows
	 */
	public function fetchColumn($index = 0);

	/**
	 * Return the entire resultset as an associative array of indexed rows.
	 *
	 * The specified index will be omitted from the row. If no index is
	 * supplied, then the first column will be used.
	 *
	 * @param integer $index  The column index to use as the key.
	 *
	 * @return array       An indexed array of indexed rows
	 */
	public function fetchKeyedAsArray($index = 0);

	/**
	 * Return the entire resultset as an indexed array, using the return value
	 * of the callback on each row.
	 *
	 * @param  callable $callback  A function to use to map the value
	 *
	 * @return array
	 */
	public function fetchAllWithCallback(callable $callback);

	/**
	 * Return the entire resultset as an associative array, using the return
	 * value of the callback on each row.
	 *
	 * @param  callable $callback  A function to use to map the value
	 *
	 * @return array
	 */
	public function fetchKeyedWithCallback(callable $callback);

	/**
	 * Return an indexed iterator for the resultset represented by an
	 * associative array.
	 *
	 * @return iterator
	 */
	public function getIterator() : Traversable;

	/**
	 * Return an associative iterator for the resultset represented by an
	 * associative array.
	 *
	 * The specified key will be omitted from the row. If no key is supplied,
	 * then the first column is used.
	 *
	 * @param  string $key The column to use as the key
	 *
	 * @return iterator
	 */
	public function getKeyedIterator($key = '');

	/**
	 * Return an indexed iterator for the resultset represented by an indexed
	 * array.
	 *
	 * @return iterator
	 */
	public function getArrayIterator();

	/**
	 * Return an indexed iterator for a single column in the resultset.
	 *
	 * @param integer $index  The 0-based column index to return
	 *
	 * @return iterator
	 */
	public function getColumnIterator($index = 0);

	/**
	 * Return an associative iterator for the resultset represented by an
	 * indexed array.
	 *
	 * The specified index will be omitted from the row. If no index is
	 * supplied, then the first column is used.
	 *
	 * @param  integer $index The column index to use as the key
	 *
	 * @return iterator
	 */
	public function getKeyedArrayIterator($index = 0);

	/**
	 * Return an indexed iterator for the resultset, using the return value
	 * of the callback on each row.
	 *
	 * @param  callable $callback  A function to use to map the value
	 *
	 * @return iterator
	 */
	public function getCallbackIterator(callable $callback);

	/**
	 * Return an associative iterator for the resultset, using the return value
	 * of the callback on each row.
	 *
	 * If no key is supplied, then the first column is used.
	 *
	 * @param  callable $callback  A function to use to map the value
	 *
	 * @return iterator
	 */
	public function getKeyedCallbackIterator(callable $callback);
}
