<?php namespace mef\Db\Driver\Exception;

use Exception;
use Throwable;

abstract class AbstractSqlException extends Exception
{
	/**
	 * @var string
	 */
	protected $sql;

	/**
	 * Constructor
	 *
	 * @param string    $sql         the SQL that triggered the error
	 * @param string    $message     a message describing the error
	 * @param string    $code        the message from the driver
	 * @param Throwable $previous    the previous exception (if any)
	 */
	public function __construct($sql, $message = '', $code = '', Throwable $previous = null)
	{
		$this->sql = (string) $sql;

		// Must directly set the code like this because the PHP Exception class
		// requires an integer in the constructor. However, it will ignore the
		// value in the constructor if $this->code is already set.
		$this->code = $code;

		parent::__construct($message, 0, $previous);
	}

	static public function fromException(Exception $exception, string $sql)
	{
		return new static($sql, $exception->getMessage(), $exception->getCode(), $exception);
	}

	/**
	 * Return the SQL that triggered the error.
	 *
	 * @return string
	 */
	public function getSql()
	{
		return $this->sql;
	}
}