<?php namespace mef\Db\Driver\DataProvider;

/**
 * Uses an array of arrays to provide data to queries. The first
 * query gets the first array, etc.
 */
class SequentialArrayDataProvider implements DataProviderInterface
{
	/**
	 * @var array
	 */
	private $datasets;

	/**
	 * @var integer
	 */
	private $counter = 0;

	/**
	 * @var array
	 */
	private $lastData = [];

	/**
	 * Constructor
	 *
	 * The array must contain sub-arrays, each which represent
	 * data to return for a given query. The array may be sparse. The
	 * most recent index e.g.,
	 *
	 * [
	 *   0 => [['a' => 1, 'b' => 1]],
	 *   5 => [['a' => 2, 'b' => 2]],
	 *   7 => []
	 * ]
	 *
	 * The first five queries (0..4) will return ['a' => 1, 'b' => 1]
	 * Then the next two (5..6) will return ['a' => 2, 'b' => 2]
	 * The remainder will return []
	 *
	 * @param array $datasets
	 */
	public function __construct(array $datasets = [])
	{
		$this->datasets = $datasets;
	}

	/**
	 * Iterate over the data.
	 *
	 * @param  string $sql
	 *
	 * @return Iterator
	 */
	public function getDataForQuery($sql)
	{
		if (isset($this->datasets[$this->counter]) === true)
		{
			$data = $this->datasets[$this->counter];
			$this->lastData = $data;
		}
		else
		{
			$data = $this->lastData;
		}

		foreach ($data as $row)
		{
			yield $row;
		}

		++$this->counter;
	}

	/**
	 * Return the number of affected rows for the query.
	 *
	 * @param  string $sql
	 *
	 * @return integer
	 */
	public function executeQuery($sql)
	{
		return 0;
	}

	/**
	 * Iterate over the data.
	 *
	 * @param  string $sql
	 * @param  array $parameters
	 *
	 * @return Iterator
	 */
	public function getDataForStatement($sql, array $parameters)
	{
		foreach ($this->getDataForQuery($sql) as $row)
		{
			yield $row;
		}
	}

	/**
	 * Return the number of affected rows for the query.
	 *
	 * @param  string $sql
	 * @param  array $parameters
	 *
	 * @return integer
	 */
	public function executeStatement($sql, array $parameters)
	{
		return 0;
	}
}