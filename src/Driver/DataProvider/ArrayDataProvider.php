<?php namespace mef\Db\Driver\DataProvider;

use mef\Db\Statement\DataProviderStatement;

/**
 * Uses an array to provide data to all queries,
 * regardless of the SQL.
 */
class ArrayDataProvider extends SequentialArrayDataProvider
{
	/**
	 * Constructor
	 *
	 * @param array $data
	 */
	public function __construct(array $data = [])
	{
		parent::__construct([$data]);
	}
}