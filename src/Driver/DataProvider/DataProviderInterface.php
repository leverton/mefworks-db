<?php namespace mef\Db\Driver\DataProvider;

interface DataProviderInterface
{
	/**
	 * Return a generator that yields an associative array that represents
	 * a single row from the database.
	 *
	 * @param  string $sql
	 *
	 * @return \Iterator
	 */
	public function getDataForQuery($sql);

	/**
	 * Return the number of affected rows for the query.
	 *
	 * @param  string $sql
	 *
	 * @return integer
	 */
	public function executeQuery($sql);

	/**
	 * Return a generator that yields an associative array that represents
	 * a single row from the database.
	 *
	 * @param  string $sql
	 * @param  array $parameters
	 *
	 * @return \Iterator
	 */
	public function getDataForStatement($sql, array $parameters);

	/**
	 * Return the number of affected rows for the query.
	 *
	 * @param  string $sql
	 * @param  array $parameters
	 *
	 * @return integer
	 */
	public function executeStatement($sql, array $parameters);
}