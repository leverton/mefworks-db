<?php namespace mef\Db\Driver;

use RuntimeException;

use mef\Db\TransactionDriver\TransactionDriverInterface;

abstract class AbstractDriver implements DriverInterface
{
	/**
	 * @var \mef\Db\TransactionDriver\TransactionDriverInterface
	 */
	protected $transaction;

	/**
	 * Return the transaction engine.
	 *
	 * @return \mef\Db\TransactionDriver\TransactionDriverInterface
	 */
	public function getTransactionDriver()
	{
		return $this->transaction;
	}

	/**
	 * Set the transaction engine.
	 *
	 * @param \mef\Db\TransactionDriver\TransactionDriverInterface $transaction
	 */
	public function setTransactionDriver(TransactionDriverInterface $transaction)
	{
		$this->transaction = $transaction;
	}

	/**
	 * Begin a transaction.
	 */
	public function startTransaction()
	{
		if ($this->transaction === null)
		{
			throw new RuntimeException('Must call setTransactionDriver first.');
		}

		$this->transaction->start();
	}

	/**
	 * Commit the transaction.
	 */
	public function commit()
	{
		if ($this->transaction === null)
		{
			throw new RuntimeException('Must call setTransactionDriver first.');
		}

		$this->transaction->commit();
	}

	/**
	 * Rollback the transaction.
	 */
	public function rollBack()
	{
		if ($this->transaction === null)
		{
			throw new RuntimeException('Must call setTransactionDriver first.');
		}

		$this->transaction->rollBack();
	}

	/**
	 * Return true if inside a transaction.
	 *
	 * @return boolean
	 */
	public function inTransaction(): bool
	{
		return $this->transaction !== null && $this->transaction->exists();
	}
}