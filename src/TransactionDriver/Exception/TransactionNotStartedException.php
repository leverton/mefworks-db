<?php namespace mef\Db\TransactionDriver\Exception;

use Exception;

class TransactionNotStartedException extends Exception
{
    public function __construct(Exception $previous = null)
    {
        parent::__construct('Transaction not started.', 0, $previous);
    }
}