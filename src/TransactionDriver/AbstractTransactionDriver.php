<?php namespace mef\Db\TransactionDriver;

use mef\Db\Driver\DriverInterface;

abstract class AbstractTransactionDriver implements TransactionDriverInterface
{
	/**
	 * @var DriverInterface
	 */
	protected $db;

	/**
	 * Constructor
	 *
	 * @param \mef\Db\Driver\DriverInterface $db
	 */
	public function __construct(DriverInterface $db)
	{
		$this->setDatabaseDriver($db);
	}

	/**
	 * Return the database driver.
	 *
	 * @return \mef\Db\Driver\DriverInterface
	 */
	final public function getDatabaseDriver()
	{
		return $this->db;
	}

	/**
	 * Set the database driver.
	 *
	 * @param \mef\Db\Driver\DriverInterface $db
	 */
	public function setDatabaseDriver(DriverInterface $db)
	{
		$this->db = $db;
	}
}