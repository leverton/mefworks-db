<?php namespace mef\Db\TransactionDriver;

use mef\Db\TransactionDriver\Exception\TransactionNotStartedException;
use mef\Db\TransactionDriver\Exception\CommitException;

class EmulatedNestedTransactionDriver extends AbstractTransactionDriver
{
	private $transDepth = 0, $rollback = false;

	/**
	 * Begin a transaction.
	 *
	 * To provide consistent behavior, nested transactions are emulated,
	 * regardless if the underlying driver supports it. However, some
	 * functionality is driver-dependent. e.g., In MySQL transactions are
	 * not supported for MyISAM tables, and certain queries trigger an
	 * implicit commit.
	 */
	final public function start($name = null)
	{
		if (++$this->transDepth === 1)
		{
			$this->doStart();
		}
	}

	/**
	 * Commits a transaction.
	 */
	final public function commit()
	{
		if ($this->transDepth === 0)
		{
			throw new TransactionNotStartedException;
		}

		if ($this->rollback === true)
		{
			throw new CommitException('An inner transaction was rolled back.');
		}

		$this->transDepth--;

		if ($this->transDepth === 0)
		{
			$this->doCommit();
		}
	}

	/**
	 * Rolls back a transaction.
	 *
	 * In the case of nested transactions, if an inner transaction
	 * is rolled back, then ALL transactions will be rolled back at the
	 * time of the outermost commit or roll back operation.
	 *
	 * For any other behavior, database dependent code must be used.
	 */
	final public function rollBack()
	{
		if ($this->transDepth === 0)
		{
			throw new TransactionNotStartedException;
		}

		$this->transDepth--;

		if ($this->transDepth === 0)
		{
			$this->doRollBack();
			$this->rollback = false;
		}
		else
		{
			$this->rollback = true;
		}
	}

	/**
	 * Return true if a transaction has started.
	 *
	 * @return boolean
	 */
	public function exists(): bool
	{
		return $this->transDepth > 0;
	}

	/**
	 * Start the transaction.
	 *
	 * Can be overridden by subclasses.
	 */
	protected function doStart()
	{
		$this->db->execute('BEGIN');
	}

	/**
	 * Commit the transaction.
	 *
	 * Can be overridden by subclasses.
	 */
	protected function doCommit()
	{
		$this->db->execute('COMMIT');
	}

	/**
	 * Roll back the transaction.
	 *
	 * Can be overridden by subclasses.
	 */
	protected function doRollBack()
	{
		$this->db->execute('ROLLBACK');
	}
}