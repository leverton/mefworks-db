<?php namespace mef\Db\TransactionDriver;

interface TransactionDriverInterface
{
	public function start($name = '');

	public function commit();

	public function rollBack();

	public function exists(): bool;
}