<?php namespace mef\Db\Statement;

use mysqli_stmt;
use Exception;
use InvalidArgumentException;

use mef\Db\Statement;
use mef\Db\RecordSet\MySqliRecordSet;

class MySqliStatement extends AbstractStatement
{
	/**
	 * @var \mysqli_stmt
	 */
	protected $st;

	/**
	 * @var array
	 */
	protected $namedParameters;

	/**
	 * @var string
	 */
	protected $boundParametersFormat = [];

	/**
	 * An array of bound parameters.
	 *
	 * @var array
	 */
	protected $boundParameters = [];

	static private $map = [
		Statement::AUTOMATIC => 's',
		Statement::BOOLEAN => 'i',
		Statement::NULL => 's',
		Statement::INTEGER => 'i',
		Statement::STRING => 's',
		Statement::BLOB => 'b',
	];

	/**
	 * Constructor
	 *
	 * @param \mysqli_stmt $st
	 * @param array $namedParameters
	 */
	public function __construct(mysqli_stmt $st, array $namedParameters)
	{
		$this->st = $st;
		$this->namedParameters = $namedParameters;
	}

	/**
	 * Return the underlying mysqli_stmt.
	 *
	 * @return \mysqli_stmt
	 */
	public function getMySqliStatement()
	{
		return $this->st;
	}

	/**
	 * Return the emulated, named parameters.
	 *
	 * @return array
	 */
	public function getNamedParameters()
	{
		return $this->namedParameters;
	}

	/**
	 * Return the currently bound parameters.
	 *
	 * @return array
	 */
	public function getBoundParameters()
	{
		return $this->boundParameters;
	}

	/**
	 * Bind parameter by reference.
	 *
	 * @param  string $key
	 * @param  string &$val
	 * @param  integer $type
	 */
	public function bindParameter($key, &$val, $type = Statement::AUTOMATIC)
	{
		if (is_string($key) === true)
		{
			if (isset($this->namedParameters[$key]) === false)
			{
				throw new InvalidArgumentException('Unknown parameter ' . $key);
			}

			foreach ($this->namedParameters[$key] as $index)
			{
				$this->boundParametersFormat[$index] = self::$map[$type];
				$this->boundParameters[$index] = &$val;
			}
		}
		else if (is_integer($key) === true)
		{
			$this->boundParametersFormat[$key] = self::$map[$type];
			$this->boundParameters[$key] = &$val;
		}
		else
		{
			throw new InvalidArgumentException("\$key must be a string or integer");
		}
	}

	/**
	 * Bind parameter by value.
	 *
	 * @param  string $key
	 * @param  string $val
	 * @param  integer $type
	 */
	public function setParameter($key, $val, $type = Statement::AUTOMATIC)
	{
		$this->bindParameter($key, $val, $type);
	}

	/**
	 * Execute the prepared statement.
	 *
	 * Used for DELETE,INSERT,UPDATE queries.
	 *
	 * @return integer   number of affected rows
	 */
	public function execute()
	{
		if (count($this->boundParametersFormat) !== 0)
		{
			$this->st->bind_param(implode($this->boundParametersFormat), ...$this->boundParameters);
		}

		$this->st->execute();

		return $this->st->affected_rows;
	}

	/**
	 * Query the database using the prepared statement.
	 *
	 * Used for SELECT queries.
	 *
	 * @return \mef\Db\RecordSet\MySqliRecordSet
	 */
	public function query()
	{
		if (count($this->boundParametersFormat) !== 0)
		{
			$this->st->bind_param(implode($this->boundParametersFormat), ...$this->boundParameters);
		}

		$this->st->execute();

		return new MySqliRecordSet($this->st->get_result());
	}
}
