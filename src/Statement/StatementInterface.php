<?php namespace mef\Db\Statement;

use mef\Db\Statement;

interface StatementInterface
{
	/**
	 * Bind (by reference) the value to the parameter.
	 *
	 * The key is either the name of the parameter (e.g. :foo) or the index
	 * (0-based), depending on which type the statement is using.
	 *
	 * @param  string|integer $key
	 * @param  mixed          $value
	 * @param  integer        $type
	 */
	public function bindParameter($key, &$value, $type = Statement::AUTOMATIC);

	/**
	 * Bind (by reference) the values to the parameters.
	 *
	 * The $parameters array must be in key => value form, where the key is
	 * either the name of the parameter (e.g. :foo) or the index (0-based),
	 * depending on which type the statement is using.
	 *
	 * @param  array $parameters
	 * @param  array $types
	 */
	public function bindParameters(array $parameters, array $types = []);

	/**
	 * Set (by value) the value to the parameter.
	 *
	 * The key is either the name of the parameter (e.g. :foo) or the index
	 * (0-based), depending on which type the statement is using.
	 *
	 * @param  string|integer $key
	 * @param  mixed          $value
	 * @param  integer        $type
	 */
	public function setParameter($key, $value, $type = Statement::AUTOMATIC);

	/**
	 * Set (by value) the values to the parameters.
	 *
	 * The $parameters array must be in key => value form, where the key is
	 * either the name of the parameter (e.g. :foo) or the index (0-based),
	 * depending on which type the statement is using.
	 *
	 * @param  array $parameters
	 * @param  array $types
	 */
	public function setParameters(array $parameters, array $types = []);

	/**
	 * Return a buffered RecordSet for the given query.
	 *
	 * This must only be used with queries that return a record set.
	 * For DELETE, INSERT, UPDATE, etc, use DriverInterface::execute().
	 *
	 * @return \mef\DB\RecordSet\RecordSetInterface
	 */
	public function query();

	/**
	 * Execute the given query.
	 *
	 * @return integer     The number of rows affected (if supported)
	 */
	public function execute();
}
