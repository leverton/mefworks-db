<?php namespace mef\Db\Statement;

use PDO;
use PDOStatement as PHP_PDOStatement;
use Exception;

use mef\Db\Statement;
use mef\Db\RecordSet\PdoRecordSet;

class PdoStatement extends AbstractStatement
{
	/**
	 * @var \PDOStatement
	 */
	private $st;

	static private $map = [
		Statement::AUTOMATIC => PDO::PARAM_STR,
		Statement::BOOLEAN => PDO::PARAM_BOOL,
		Statement::NULL => PDO::PARAM_NULL,
		Statement::INTEGER => PDO::PARAM_INT,
		Statement::STRING => PDO::PARAM_STR,
		Statement::BLOB => PDO::PARAM_LOB,
	];

	/**
	 * Constructor
	 *
	 * @param \PDOStatement $st
	 */
	public function __construct(PHP_PDOStatement $st)
	{
		$this->st = $st;
	}

	/**
	 * Return the underlying PDO statement.
	 *
	 * @return \PDOStatement
	 */
	public function getPdoStatement()
	{
		return $this->st;
	}

	/**
	 * Bind parameter by reference.
	 *
	 * @param  string|integer $key
	 * @param  string $val
	 * @param  integer $type
	 */
	public function bindParameter($key, &$val, $type = Statement::AUTOMATIC)
	{
		if (is_integer($key) === true)
		{
			$key += 1;
		}

		$this->st->bindParam($key, $val, self::$map[$type]);
	}

	/**
	 * Bind parameter by value.
	 *
	 * @param  string|integer $key
	 * @param  string $val
	 * @param  integer $type
	 */
	public function setParameter($key, $val, $type = Statement::AUTOMATIC)
	{
		if (is_integer($key) === true)
		{
			$key += 1;
		}

		$this->st->bindValue($key, $val, self::$map[$type]);
	}

	/**
	 * Execute the prepared statement.
	 *
	 * Used for DELETE,INSERT,UPDATE queries.
	 *
	 * @return integer   number of affected rows
	 */
	public function execute()
	{
		$this->st->execute();

		return $this->st->rowCount();
	}

	/**
	 * Query the database using the prepared statement.
	 *
	 * Used for SELECT queries.
	 *
	 * @return \mef\Db\RecordSet\PdoRecordSet
	 */
	public function query()
	{
		$this->st->execute();
		return new PdoRecordSet($this->st);
	}
}
