<?php namespace mef\Db\Statement;

use mef\Db\Statement;
use mef\Db\RecordSet\IteratorRecordSet;
use mef\Db\Driver\DataProvider\DataProviderInterface;

class DataProviderStatement extends AbstractStatement
{
	/**
	 * @var \mef\Db\Driver\DataProvider\DataProviderInterface
	 */
	protected $dataProvider;

	/**
	 * @var string
	 */
	protected $sql;

	/**
	 * @var array
	 */
	protected $parameters;

	/**
	 * Constructor
	 *
	 * @param \mef\Db\Driver\DataProvider\DataProviderInterface $dataProvider
	 * @param string $sql         The SQL associated with the statement
	 * @param array  $parameters  The bound parameters
	 */
	public function __construct(DataProviderInterface $dataProvider, $sql, array $parameters)
	{
		$this->dataProvider = $dataProvider;
		$this->sql = $sql;
		$this->parameters = $parameters;
	}

	/**
	 * Return the data provider.
	 *
	 * @return \mef\Db\Driver\DataProvider\DataProviderInterface
	 */
	public function getDataProvider()
	{
		return $this->dataProvider;
	}

	/**
	 * Return the SQL string.
	 *
	 * @return string
	 */
	public function getSql()
	{
		return $this->sql;
	}

	/**
	 * Return the parameters
	 *
	 * @return array
	 */
	public function getParameters()
	{
		return $this->parameters;
	}

	/**
	 * Bind (by reference) the value to the parameter.
	 *
	 * The key is either the name of the parameter (e.g. :foo) or the index
	 * (0-based), depending on which type the statement is using.
	 *
	 * @param  string|integer $key
	 * @param  mixed          $value
	 * @param  integer        $type
	 */
	public function bindParameter($key, &$value, $type = Statement::AUTOMATIC)
	{
		$this->parameters[$key] = &$value;
	}

	/**
	 * Bind (by reference) the values to the parameters.
	 *
	 * The $parameters array must be in key => value form, where the key is
	 * either the name of the parameter (e.g. :foo) or the index (0-based),
	 * depending on which type the statement is using.
	 *
	 * @param  array $parameters
	 * @param  array $types
	 */
	public function bindParameters(array $parameters, array $types = [])
	{
		foreach ($parameters as $key => &$value)
		{
			$this->parameters[$key] = &$value;
		}
	}

	/**
	 * Set (by value) the value to the parameter.
	 *
	 * The key is either the name of the parameter (e.g. :foo) or the index
	 * (0-based), depending on which type the statement is using.
	 *
	 * @param  string|integer $key
	 * @param  mixed          $value
	 * @param  integer        $type
	 */
	public function setParameter($key, $value, $type = Statement::AUTOMATIC)
	{
		$this->parameters[$key] = $value;
	}

	/**
	 * Set (by value) the values to the parameters.
	 *
	 * The $parameters array must be in key => value form, where the key is
	 * either the name of the parameter (e.g. :foo) or the index (0-based),
	 * depending on which type the statement is using.
	 *
	 * @param  array $parameters
	 * @param  array $types
	 */
	public function setParameters(array $parameters, array $types = [])
	{
		foreach ($parameters as $key => $value)
		{
			$this->parameters[$key] = $value;
		}
	}

	/**
	 * Return a buffered RecordSet for the given query.
	 *
	 * This must only be used with queries that return a record set.
	 * For DELETE, INSERT, UPDATE, etc, use DriverInterface::execute().
	 *
	 * @return \mef\Db\RecordSet\IteratorRecordSet
	 */
	public function query()
	{
		return new IteratorRecordSet($this->dataProvider->getDataForStatement($this->sql, $this->parameters));
	}

	/**
	 * Execute the given query.
	 *
	 * @return integer     The number of rows affected (if supported)
	 */
	public function execute()
	{
		return $this->dataProvider->executeStatement($this->sql, $this->parameters);
	}
}